const hostJson = require("./host.json");
export const properties = {
    salt: "salt",
    getBaseUrl : () => {
        if(hostJson.host === "local"){
            return 'http://localhost:3000';
        }else{
            return 'http://13.209.168.173:3000';
        }
    }
};