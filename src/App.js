import React, { Component } from 'react';
import {observer, inject} from 'mobx-react';
import Header from './components/Header';
import Main from './components/Main';
import Footer from './components/Footer';
import Background from './components/Background';
const moment = require('moment');

require("moment/min/locales.min");
moment.locale('cs');

@inject('headerStore')
@observer
class App extends Component {
  componentDidMount (){
    const { headerStore } = this.props;
    headerStore.checkLogin();
    window.setTimeout(function() {
      document.body.classList.remove('is-preload');
    }, 200); 
  }

  render() {
      return(
        <React.Fragment>
          <div id="wrapper">
          <Header/>
            <Main/>
            <Footer/>
          </div>
          <Background/>
        </React.Fragment>
      );
  }
}
 
export default App;