import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';

@inject('loginStore', 'headerStore')
@observer
class Login extends Component {
  render() {
    const { loginStore, headerStore } = this.props;
      return(
        <article id="login" className={loginStore.article_class} style={{display:loginStore.article_display}}>
          <h2 className="major">로그인</h2>
          <form method="post" action="#">
            <div className="fields">
              <div className="field">
                <label htmlFor="hunterId">헌터ID</label>
                <input type="text" name="hunterId" id="hunterId" maxLength="100" value={loginStore.hunterId} onChange={loginStore.changeHunterId}/>
              </div>
              <div className="field">
                <label htmlFor="password">비밀번호</label>
                <input type="password" name="password" maxLength="30" value={loginStore.password} onChange={loginStore.changePassword}/>
              </div>
            </div>
            <ul className="actions">
              <li><input type="submit" value="로그인" className="primary" onClick={(e) => loginStore.eventlogin(e, headerStore)}/></li>
              <li><input type="reset" value="취소" onClick={headerStore.eventMainHide}/></li>
            </ul>
          </form>
          <div className="close" onClick={headerStore.eventMainHide}>Close</div>
        </article>
      );
  }
}
 
export default Login;