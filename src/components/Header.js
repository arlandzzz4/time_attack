import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';

@inject('headerStore')
@observer
class Header extends Component {
  render() {
    const { headerStore } = this.props;
    document.getElementById('body').onclick = function(e){
      if (e.target.id === 'wrapper' || e.target.id === 'main'){
        headerStore.eventMainHide();
      }
    };
    
    let login_display = 'none';
    let logout_display = '';
    if (headerStore.isLogin) {
        login_display = '';
        logout_display = 'none';
    }
    
      return(
        <header id="header" style={{display:headerStore.header_display}}>
          <div className="logo">
            <span className="icon fa-github-square"></span>
          </div>
          <div className="content">
            <div className="inner">
              <h1>야옹이보험단</h1>
              <p>몬스터헌터 아이스본 수렵단 야옹이보험단의 타임어택 대회 웹페이지입니다.<br />
              해당 웹페이지는 현재 테스트 중 입니다.</p>
            </div>
          </div>
          <nav>
            <ul>
              <li style={{display:login_display}}><a href="#" onClick={headerStore.appContestShow}>대회등록</a></li>
              <li><a href="#" onClick={headerStore.appContestListShow}>대회목록</a></li>
              <li style={{display:logout_display}}><a href="#" onClick={headerStore.appRegistShow}>헌터등록</a></li>
              <li style={{display:logout_display}}><a href="#" onClick={headerStore.appLoginShow}>로그인</a></li>
              <li style={{display:login_display}}><a href="#" onClick={(e) => headerStore.eventLogout(e)}>로그아웃</a></li>
            </ul>
          </nav>
        </header>
      );
  }
}
 
export default Header;