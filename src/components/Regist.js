import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';

@inject('registStore', 'headerStore')
@observer
class Regist extends Component {
  render() {
    const { registStore, headerStore } = this.props;
      return(
        <article id="regist" className={registStore.article_class} style={{display:registStore.article_display}}>
          <h2 className="major">헌터등록</h2>
          <form method="post" action="#">
            <div className="fields">
              <div className="field">
                <label htmlFor="hunter_name">헌터명</label>
                <input type="text" name="hunter_name" id="hunter_name" maxLength="100" value={registStore.hunterName} onChange={registStore.changeHunterName}/>
              </div>
              <div className="field">
                <label htmlFor="hunter_id">헌터ID</label>
                <input type="text" name="hunter_id" id="hunter_id" maxLength="30" value={registStore.hunterId} onChange={registStore.changeHunterId}/>
              </div>
              <div className="field half">
                <label htmlFor="password">비밀번호</label>
                <input type="password" name="password" id="password" maxLength="30" value={registStore.password} onChange={registStore.changePassword}/>
              </div>
              <div className="field half">
                <label htmlFor="password">비밀번호2</label>
                <input type="password" name="password2" id="password2" maxLength="30" value={registStore.password2} onChange={registStore.changePassword2}/>
              </div>
            </div>
            <ul className="actions">
              <li><input type="submit" value="등록" className="primary" onClick={(e) => registStore.eventRegistHunter(e, headerStore)}/></li>
              <li><input type="reset" value="취소" onClick={headerStore.eventMainHide}/></li>
            </ul>
          </form>
          <div className="close" onClick={headerStore.eventMainHide}>Close</div>
        </article>
      );
  }
}
 
export default Regist;