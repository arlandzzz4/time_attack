import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';

@inject('footerStore')
@observer
class Footer extends Component {
  render() {
    const { footerStore } = this.props;
      return(
        <footer id="footer" style={{display:footerStore.article_display}}>
          <p className="copyright">&copy; Untitled. Design: <a href="https://html5up.net">HTML5 UP & Develope by sincelife</a>.</p>
        </footer>
      );
  }
}
 
export default Footer;