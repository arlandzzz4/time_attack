import React, { Component, Fragment } from 'react';
import { observer, inject } from 'mobx-react';
import ReactPlayer from 'react-player';
import {common} from '../etc/common';

const moment = require('moment');

@inject('contestDetailStore', 'headerStore')
@observer
class ContestDetail extends Component {
	render() {
		const { contestDetailStore, headerStore } = this.props;
		if(headerStore.isLogin){
			if(contestDetailStore.contestDetail){
				const d = new Date();
				let day = moment(d);
				day.format("YYYY-MM-DD");
				const start = moment(contestDetailStore.contestDetail[0].contest_start,"YYYY-MM-DD");
				const end = moment(contestDetailStore.contestDetail[0].contest_end + " 23:59:59","YYYY-MM-DD HH:mm:ss");
				const dayToStart = day.diff(start);
				const dayToEnd = day.diff(end);

				if(dayToStart >= 0 && dayToEnd <= 0){
					headerStore.isProgress = true;
				}
			}
		}
      return(
		<article id="ContestDetail" className={contestDetailStore.article_class} style={{display:contestDetailStore.article_display}}>
			<h2 className="major">대회 상세</h2>
			{
			contestDetailStore.contestDetail != null ?
            <div className="fields">
			  <h3>대회명</h3>
              <p>{contestDetailStore.contestDetail[0].contest_title}</p>
			  <h3>멀티 인원</h3>
			  <p>{contestDetailStore.contestDetail[0].multiplayer} 명</p>
			  <h3>대회 기간</h3>
			  <p>{contestDetailStore.contestDetail[0].contest_start}~{contestDetailStore.contestDetail[0].contest_end}</p>	
			  <h3>대회 설정</h3>
			  <blockquote>
			  	{contestDetailStore.contestDetail[0].description}
			  </blockquote>
            </div>
			: <div className="fields"></div>
			}
			<section>
				<h3 className="major">대회 순위</h3>
				<div className="table-wrapper">
					<table>
						<thead>
							<tr>
								<th>순위</th>
								<th>헌터명</th>
								<th>기록</th>
								<th>{headerStore.isLogin?"수정":""}</th>
							</tr>
						</thead>
						<tbody>
						{
							(contestDetailStore.contestDetail != null && contestDetailStore.contestDetail[0].record_list != null
							&& contestDetailStore.chk === 'Y') ?
							contestDetailStore.contestDetail.map((contest, index) => {
								return contest.multiplayer === '1' ?
								<RecordRow key={index} index={index} record={contest.record_list} event={headerStore} contest_id={contestDetailStore.contestDetail[0].contest_id}/>
								: <RecordRow2 key={index} index={index} record={contest.record_list} event={headerStore} contest_id={contestDetailStore.contestDetail[0].contest_id}/>
							} )
							: <tr></tr>
						}
						</tbody>
					</table>
				</div>
			</section>
			<div className="close" onClick={headerStore.eventContestDetailHide}>Close</div>
		</article>
      );
  }
}

class RecordRow extends React.Component {
    render() {
		let itsme = false;
		if(this.props.event.isLogin){
			//날짜 체크
			if(this.props.event.isProgress){
				if(this.props.record.hunter_id === this.props.event.hunter_id){
					itsme = true;
				}
			}
		}
		if(itsme){
			return(
				<Fragment>
					<tr>
						<td>{this.props.index + 1}</td>
						<td>{this.props.record.hunter_name}</td>
						<td>{this.props.record.record_m}:{this.props.record.record_s}:{this.props.record.record_ms}</td>
						<td onClick={() => this.props.event.appContestRecordShow(this.props.contest_id)}>수정</td>
					</tr>
					{common.getBaseUrl(this.props.record.url)?
					<tr><td colSpan={4}><ReactPlayer url={this.props.record.url} width="500px" height="300px"/>
					</td></tr>
					:this.props.record.url?<tr><td colSpan={4}><img src={this.props.record.url} onClick={() => window.open(this.src)} width={535} height={300} /></td></tr>:""
					}
				</Fragment>
				);
		}else
        return(
			<Fragment>
				<tr>
					<td>{this.props.index + 1}</td>
					<td>{this.props.record.hunter_name}</td>
					<td>{this.props.record.record_m}:{this.props.record.record_s}:{this.props.record.record_ms}</td>
					<td></td>
				</tr>
				{common.getBaseUrl(this.props.record.url)?
				<tr><td colSpan={4}><ReactPlayer url={this.props.record.url} width="500px" height="300px"/>
				</td></tr>
				:this.props.record.url?<tr><td colSpan={4}><img src={this.props.record.url} onClick={() => window.open(this.props.record.url)} width={535} height={300} /></td></tr>:""
				}
			</Fragment>
            );
    }
}

class RecordRow2 extends React.Component {
    render() {
		let itsme = false;
		if(this.props.event.isLogin){
			//날짜 체크
			if(this.props.event.isProgress){
				if(this.props.record.hunter_id.hunter1 === this.props.event.hunter_id || this.props.record.hunter_id.hunter2 === this.props.event.hunter_id){
					itsme = true;
				}
			}
		}
		if(itsme){
			return(
				<Fragment>
					<tr>
						<td>{this.props.index + 1}</td>
						<td>{this.props.record.team_name}</td>
						<td>{this.props.record.record_m}:{this.props.record.record_s}:{this.props.record.record_ms}</td>
						<td><input type="reset" value="수정" onClick={() => this.props.event.appContestRecordShow(this.props.contest_id)}></input></td>
					</tr>
					{common.getBaseUrl(this.props.record.url)?
					<tr><td colSpan={4}><ReactPlayer url={this.props.record.url} width="500px" height="300px"/>
					</td></tr>
					:this.props.record.url?<tr><td colSpan={4}><img src={this.props.record.url} onClick={() => window.open(this.src)} width={535} height={300} /></td></tr>:""
					}
				</Fragment>
				);
		}else
        return(
			<Fragment>
				<tr>
					<td>{this.props.index + 1}</td>
					<td>{this.props.record.team_name}</td>
					<td>{this.props.record.record_m}:{this.props.record.record_s}:{this.props.record.record_ms}</td>
					<td></td>
				</tr>
				{common.getBaseUrl(this.props.record.url)?
				<tr><td colSpan={4}><ReactPlayer url={this.props.record.url} width="500px" height="300px"/>
				</td></tr>
				:this.props.record.url?<tr><td colSpan={4}><img src={this.props.record.url} onClick={() => window.open(this.props.record.url)} width={535} height={300} /></td></tr>:""
				}
			</Fragment>
            );
    }
}

export default ContestDetail;