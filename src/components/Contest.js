import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import {DateRangeInput} from '@datepicker-react/styled';

import "react-datepicker/dist/react-datepicker.css";

@inject('contestStore', 'headerStore')
@observer
class Contest extends Component {
  
  render() {
      const { contestStore, headerStore } = this.props;

      return(
        <article id="contest" className={contestStore.article_class} style={{display:contestStore.article_display}}>
          <h2 className="major">대회개최</h2>
          <form method="post" action="#">
            <div className="fields">
              <div className="field">
                <label htmlFor="contest_title">대회명</label>
                <input type="text" name="contest_title" value={contestStore.contestTitle} onChange={contestStore.changeContestTitle} maxLength="100"/>
              </div>
              <div className="field half">
                <input type="radio" id="multiplayer1" name="multiplayer" value="1" checked={contestStore.multiplayer1} onChange={(e) => contestStore.changeMultiplayer(e, 1)}/>
                <label htmlFor="multiplayer1">1인</label>
              </div>
              <div className="field half">
                <input type="radio" id="multiplayer2" name="multiplayer" value="2" disabled={true} checked={contestStore.multiplayer2} onChange={(e)=> contestStore.changeMultiplayer(e, 2)}/>
                <label htmlFor="multiplayer2">2인</label>
              </div>
              <div className="field">
                <label htmlFor="contest_start">대회기간</label>
                <style>{"\
          .dFPOUj{\
                display: grid;\
                grid-template-columns: repeat(2, 1fr);\
                grid-gap: 0 32px;\
                padding-right: 0px;\
                overflow: hidden;\
                height: 100%;\
          }\
        "}</style>                
                <DateRangeInput 
                  onDatesChange={data => contestStore.onDatesChange(data)}
                  onFocusChange={focusedInput => contestStore.onFocusChange(focusedInput)}
                  onClose={(e) => console.log("onClose : ",e)}
                  startDate={contestStore.contestStart} 
                  endDate={contestStore.contestEnd}
                  focusedInput={contestStore.focusedInput}
                  displayFormat={'yyyy-MM-dd'}
                  showResetDates={false}
                  showClose={false}
                  showSelectedDates={false}
                  />
                  
              </div>
              <div className="field">
                <label htmlFor="description">대회 설명</label>
                <textarea name="description" id="description" rows="4" value={contestStore.description} onChange={contestStore.changeDescription}></textarea>
              </div>
            </div>
            <ul className="actions">
              <li><input type="submit" value="등록" className="primary" onClick={(e) => contestStore.eventRegistCotest(e, headerStore)}/></li>
              <li><input type="reset" value="취소" onClick={headerStore.eventMainHide}/></li>
            </ul>
          </form>
          <div className="close" onClick={headerStore.eventMainHide}>Close</div>
        </article>
      );
  }
}

export default Contest;