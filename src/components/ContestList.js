import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
const moment = require('moment');


@inject('contestListStore', 'headerStore')
@observer
class ContestList extends Component {
	render() {
		const { contestListStore, headerStore } = this.props;
      return(
		<article id="contestList" className={contestListStore.article_class} style={{display:contestListStore.article_display}}>
			<section>
				<h3 className="major">대회 목록</h3>
				<div className="table-wrapper">
					<table>
						<thead>
							<tr>
								<th>순서</th>
								<th>대회명</th>
								<th>진행여부</th>
							</tr>
						</thead>
						<tbody>
						{
							contestListStore.contestList != null ?
								contestListStore.contestList.map(contest => (<ContestRow key={contest._id} contest={contest} event={headerStore} contestListStore={contestListStore}/>) )
								: <tr></tr>
						}
						</tbody>
					</table>
				</div>
			</section>
			<div className="close" onClick={headerStore.eventMainHide}>Close</div>
		</article>
      );
  }
}

class ContestRow extends React.Component {
    render() {
		const d = new Date();
		let day = moment(d);
		day.format("YYYY-MM-DD");

		const start = moment(this.props.contest.contest_start,"YYYY-MM-DD");
		const end = moment(this.props.contest.contest_end + " 23:59:59","YYYY-MM-DD HH:mm:ss");
		const dayToStart = day.diff(start);
		const dayToEnd = day.diff(end);

		let isJoin = false;
		if(this.props.event.isLogin){
			const recordList = this.props.contest.record_list;
			const length = recordList.length;
			let record;
			for(let i = 0 ; i<length ; i++){
				record = recordList[i];
				if(record.hunter_id === this.props.event.hunter_id){
					isJoin = true;
				}
			}
		}

		let tag = "";
		if(dayToStart > 0 && dayToEnd > 0){
			tag  = "종료";
		}else if(dayToStart >= 0 && dayToEnd <= 0){
			if(this.props.event.isLogin){
				if(isJoin){
					tag  = "참여 중";
				}else{
					tag  = "참여하기";
				}
			}else{
				tag  = "진행 중";
			}
		}else{
			tag  = "진행 전";
		}

		if(tag === "참여하기"){
			return(
				<tr>
					<td>{this.props.contest.contest_id}</td>
					<td onClick={() => this.props.event.appContestDetailShow(this.props.contest.contest_id)}>{this.props.contest.contest_title}</td>
					<td>
						<input type="reset" value={tag} onClick={(e) => this.props.contestListStore.contestEnter(e, this.props.contest.contest_id)}></input>
					</td>
				</tr>
				);
		}else
			return(
				<tr>
					<td>{this.props.contest.contest_id}</td>
					<td onClick={() => this.props.event.appContestDetailShow(this.props.contest.contest_id)}>{this.props.contest.contest_title}</td>
					<td>{tag}</td>
				</tr>
				);
    }
}
export default ContestList;