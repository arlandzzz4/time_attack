import React, { Component } from 'react';
import Contest from './Contest';
import ContestList from './ContestList';
import ContestDetail from './ContestDetail';
import ContestRecord from './ContestRecord';
import Regist from './Regist';
import Login from './Login';
import { observer, inject } from 'mobx-react';

@inject('mainStore')
@observer
class Main extends Component {
  render() {
    const { mainStore } = this.props;
      return(
        <div id="main" style={{display:mainStore.main_display}}>
        <Contest/>
        <ContestList/>
        <ContestDetail/>
        <ContestRecord/>
        <Regist/>
        <Login/>
        </div>
      );
  }
}
 
export default Main;