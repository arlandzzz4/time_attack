import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';

@inject('contestRecordStore', 'headerStore')
@observer
class ContestRecord extends Component {
	render() {
		const { contestRecordStore, headerStore } = this.props;
      return(
		<article id="ContestRecord" className={contestRecordStore.article_class} style={{display:contestRecordStore.article_display}}>
			<h2 className="major">기록 등록</h2>
			<form method="post" action="#">
				<div className="field">
				<label for="record_m">기록 (분:초:밀리초)</label>
				<p>
					<input type="number" name="record_m" style={{width:'50px',color:'#000'}} min="0" max="99" value={contestRecordStore.record_m} onChange={contestRecordStore.changeRecordM} maxLength="2"/>:
					<input type="number" name="record_s"  style={{width:'50px',color:'#000'}} min="0" max="99" value={contestRecordStore.record_s} onChange={contestRecordStore.changeRecordS} maxLength="2"/>:
					<input type="number" name="record_ms"  style={{width:'50px',color:'#000'}} min="0" max="999" value={contestRecordStore.record_ms} onChange={contestRecordStore.changeRecordMS}  maxLength="3"/>
				</p>
				</div>
				<div className="field">
				<label for="weapone">무기</label>
				<p>
					<select name="weapon" value={contestRecordStore.weapon} onChange={contestRecordStore.changeWeapon}>
						<option value="?">?</option>
						<option value="대검">대검</option>
						<option value="태도">태도</option>
						<option value="한손검">한손검</option>
						<option value="쌍검">쌍검</option>
						<option value="해머">해머</option>
						<option value="수렵피리">수렵피리</option>
						<option value="랜스">랜스</option>
						<option value="건랜스">건랜스</option>
						<option value="슬래시액스">슬래시액스</option>
						<option value="차지액스">차지액스</option>
						<option value="조충곤">조충곤</option>
						<option value="라이트보우건">라이트보우건</option>
						<option value="헤비보우건">헤비보우건</option>
						<option value="활">활</option>
					</select>
				</p>
				</div>
				<div className="field">
				<label for="url">유튜트/이미지 경로</label>
				<p>
					<input type="text" name="url" value={contestRecordStore.url} onChange={contestRecordStore.changeUrl} maxLength="5000"/>
				</p>
				</div>
				<ul className="actions">
					<li><input type="submit" value="등록" className="primary" onClick={(e) => contestRecordStore.eventUpdateRecord(e, headerStore)}/></li>
					<li><input type="reset" value="취소" onClick={headerStore.eventContestRecordHide}/></li>
				</ul>
			</form>
			<div className="close" onClick={headerStore.eventContestRecordHide}>Close</div>
		</article>
      );
  }
}

export default ContestRecord;