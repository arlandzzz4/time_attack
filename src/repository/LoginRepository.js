import {instance} from './Repository';

class LoginRepository {
    
  async login(hunterId, password) {
    const list = await instance.post('cm/login', {
      hunter_id : hunterId
      , password : password
    });
    return await list.data;
  }

  async checkLogin() {
    const list = await instance.post('cm/checkLogin');
    return await list.data;
  }

  async logout() {
    const list = await instance.post('cm/logout');
    return await list.data;
  }
}
export default new LoginRepository();