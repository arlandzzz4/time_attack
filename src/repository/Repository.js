import axios from 'axios';
import { properties } from '../etc/properties.js';

export const instance = axios.create({
  baseURL: properties.getBaseUrl(),
  timeout: 30000,
  withCredentials: true
});

// // Add a request interceptor
// instance.interceptors.request.use(async config => {
//   //session
//   //console.log('axios request : ', config);
//   //const [cookies] = useCookies(['session']);
//   console.log('axios request : ', config);
//   //console.log('axios request : ', cookies.load('connect.sid'));
//   // Do something before request is sent
//   return config;
// }, function (error) {
//   // Do something with request error
//   console.log('axios error : ', error);
//   return Promise.reject(error);
// });

// // Add a response interceptor
// instance.interceptors.response.use( response => {
//   // Any status code that lie within the range of 2xx cause this function to trigger
//   // Do something with response data
//   console.log('axios response : ', response.request);
//   //console.log('axios response : ', cookies.load('connect.sid'));
  
//   return response;
// }, async function (error) {
//   console.log('에러일 경우', error.config);
//     const errorAPI = error.config;
//     if(error.response.data.status===401 && errorAPI.retry===undefined){
//       errorAPI.retry = true;
//       console.log('토큰이 이상한 오류일 경우');
//       //await refreshToken();
//       return await axios(errorAPI);
//     }
//   // Any status codes that falls outside the range of 2xx cause this function to trigger
//   // Do something with response error
//   return Promise.reject(error);
// });