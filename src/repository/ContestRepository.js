import {instance} from './Repository';

class ContestRepository {

async getContestList() {
    const list = await instance.get('ct/getContestList');
    return await list.data;
  }

async getContestDetail(contest_id) {
    const list = await instance.get('ct/getContestDetail?contest_id='+contest_id);
    return await list.data;
  }

async registContest(contestTitle, multiplayer, contestStart, contestEnd, description) {
    const list = await instance.post('ct/registContest', {
      contestTitle : contestTitle
      , multiplayer : multiplayer
      , contestStart : contestStart
      , contestEnd : contestEnd
      , description : description
    });
    return await list.data;
  }

  async contestEnter(contest_id) {
    const list = await instance.post('ct/contestEnter', {
      contest_id : contest_id
    });
    return await list.data;
  }

  async updateRecord(contest_id, record_m, record_s, record_ms, weapon, url) {
    const list = await instance.post('ct/updateRecord', {
      contest_id : contest_id
      , record_m : record_m
      , record_s : record_s
      , record_ms : record_ms
      , weapon : weapon
      , url : url
    });
    return await list.data;
  }
}
 
export default new ContestRepository();