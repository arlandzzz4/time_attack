import {instance} from './Repository';

class HunterRepository {
    
async registHunter(hunterName, hunterId, password) {
    const list = await instance.post('ht/registHunter', {
      hunterName : hunterName
      , hunterId : hunterId
      , password : password
    });
    return await list.data;
  }
}
 
export default new HunterRepository();