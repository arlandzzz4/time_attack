import HeaderStore from './HeaderStore';
import MainStore from './MainStore';
import ContestStore from './ContestStore';
import ContestListStore from './ContestListStore';
import ContestDetailStore from './ContestDetailStore';
import ContestRecordStore from './ContestRecordStore';
import RegistStore from './RegistStore';
import Logintore from './LoginStore';
import FooterStore from './FooterStore';

class RootStore{
    constructor(){
        this.headerStore = new HeaderStore(this);
        this.mainStore = new MainStore(this);
        this.contestStore = new ContestStore(this);
        this.contestListStore = new ContestListStore(this);
        this.contestDetailStore = new ContestDetailStore(this);
        this.contestRecordStore = new ContestRecordStore(this);
        this.registStore = new RegistStore(this);
        this.loginStore = new Logintore(this);
        this.footerStore = new FooterStore(this);
    }
}
export default RootStore;