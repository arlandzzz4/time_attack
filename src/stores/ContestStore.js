import { observable, action } from 'mobx';
import ContestRepository from '../repository/ContestRepository';

export default class ContestStore {
  @observable article_class = '';
  @observable article_display = 'none';
  @observable contestTitle = '';
  @observable multiplayer = '1';
  @observable multiplayer1 = true;
  @observable multiplayer2 = false;
  @observable contestStart = null;
  @observable contestEnd = null;
  @observable focusedInput = null;
  @observable description = '';
  constructor(root){
    this.root = root;
  }

  @action contestShow = () => {
    setTimeout(() => {
      this.article_class = 'active';
    }, 300);
    this.article_display = '';
  }

  @action contestHide = () => {
    this.article_class = ''; 
    this.article_display = 'none';
  }
  @action changeContestTitle = (e) => {
    this.contestTitle = e.target.value;
  }

  @action changeMultiplayer = (e, flag) => {
    this.multiplayer = flag;
    if(flag === 1){
      this.multiplayer1 = true;
      this.multiplayer2 = false;
    }else{
      this.multiplayer1 = false;
      this.multiplayer2 = true;
    }
  }

  @action changeContestStart = (e) => {
    this.contestStart = e.target.value;
  }

  @action changeContestEnd = (e) => {
    this.contestEnd = e.target.value;
  }

  @action changeDescription = (e) => {
    this.description = e.target.value;
  }

  @action eventRegistCotest = (e, headerStore) => {
    e.preventDefault();
    e.stopPropagation();
    if(this.contestTitle === null || this.contestTitle === ""){
      alert("대회명을 입력해주세요.");
      return false;
    }

    if(this.description === null || this.description === ""){
      alert("대회설명을 입력해주세요.");
      return false;
    }

    ContestRepository.registContest(this.contestTitle, this.multiplayer, this.contestStart, this.contestEnd, this.description)
        .then(data => {
          if(data.data === "0"){
            alert("등록되었습니다.");
            headerStore.eventMainHide();
          } else if(data.data === "2"){
            alert("로그인 되어 있지 않습니다.");
            headerStore.logout();
            headerStore.eventMainHide();
            headerStore.appLoginShow();
          }
        })
        .catch(err => { console.log("ContestRepository err : " , err)});
  }

  @action onDatesChange = (data) => {
    this.contestStart = data.startDate;
    this.contestEnd = data.endDate;
    this.focusedInput = data.focusedInput;
  }

  @action onFocusChange = (focusedInput) => {
    if (!this.focusedInput) {
      this.focusedInput = focusedInput;
    }
  }
}