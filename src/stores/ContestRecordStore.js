import { observable, action } from 'mobx';
import ContestRepository from '../repository/ContestRepository';
import {common} from '../etc/common';
const isImageUrl = require('is-image-url');

export default class ContestRecordStore {
  @observable article_class = '';
  @observable article_display = 'none';
  @observable contest_id;
  @observable record_m = '';
  @observable record_s = '';
  @observable record_ms = '';
  @observable weapon = '';
  @observable url = '';

  constructor(root){
    this.root = root;
  }

  @action contestRecordShow = () => {
    setTimeout(() => {
      this.article_class = 'active';
    }, 300);
    this.article_display = '';
  }

  @action contestRecordHide = () => {
    this.article_class = '';
    this.article_display = 'none';
    this.record_m = '';
    this.record_s = '';
    this.record_ms = '';
    this.weapon = '';
    this.url = '';
  }

  @action changeRecordM = (e) => {
    this.record_m = e.target.value;
  }

  @action changeRecordS = (e) => {
    this.record_s = e.target.value;
  }

  @action changeRecordMS = (e) => {
    this.record_ms = e.target.value;
  }

  @action changeWeapon = (e) => {
    this.weapon = e.target.value;
  }

  @action changeUrl = (e) => {
    this.url = e.target.value;
  }

  @action eventUpdateRecord = (e, headerStore) => {
    e.preventDefault();
    e.stopPropagation();
    if(this.record_m === null || this.record_m === ""
      || this.record_s === null || this.record_s === ""
      || this.record_ms === null || this.record_ms === ""){
      alert("기록을 입력해주세요.");
      return false;
    }

    if(this.url !== null && this.url !== ""){
      if(!common.getBaseUrl(this.url) && !isImageUrl(this.url)){
        alert("url에는 유튜브url이나 이미지url을 입력해주세요.");
        return false;
      }
    }

    ContestRepository.updateRecord(this.contest_id,  this.record_m, this.record_s, this.record_ms, this.weapon, this.url)
        .then(data => {
          if(data.data === "0"){
            alert("등록되었습니다.");
            this.contestRecordHide();
            headerStore.appContestDetailShow(this.contest_id);
          } else if(data.data === "2"){
            alert("등록중 문제가 생겼습니다.");
          }
        })
        .catch(err => { console.log("ContestRepository err : " , err)});
  }
}