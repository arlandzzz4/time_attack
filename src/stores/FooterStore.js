import { observable, action } from 'mobx';

export default class FooterStore {
  @observable footer_display = '';
  constructor(root){
    this.root = root;
  }

  @action footerShow = () => {
    this.footer_display = '';
  }

  @action footerHide = () => {
    this.footer_display = 'none';
  }
}