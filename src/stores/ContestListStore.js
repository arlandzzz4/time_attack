import { observable, action } from 'mobx';
import {asyncAction} from "mobx-utils"
import ContestRepository from '../repository/ContestRepository';

export default class ContestListStore {
  @observable article_class = '';
  @observable article_display = 'none';
  @observable contestList;
  constructor(root){
    this.root = root;
  }

  @action contestListShow = () => {
    setTimeout(() => {
      this.article_class = 'active';
    }, 300);
    this.article_display = '';
  }

  @action contestListHide = () => {
    this.article_class = '';
    this.article_display = 'none';
  }

  @asyncAction
  getContestList = () => {
     ContestRepository.getContestList()
        .then(data => this.contestList = data)
        .catch(err => { console.log("ContestListStore err : " , err)});
   }

   @asyncAction
   contestEnter = (e, contest_id, hunter_id, hunter_name) => {
        e.preventDefault();
        e.stopPropagation();
        if(contest_id === null || hunter_id === ""){
          alert("오류가 있습니다. 화면을 리프레시하고 다시 시도해주세요.");
          return false;
        }
    
        if(hunter_id === null || hunter_id === ""){
          alert("오류가 있습니다. 화면을 리프레시하고 다시 시도해주세요.");
          return false;
        }

     ContestRepository.contestEnter(contest_id, hunter_id, hunter_name)
        .then(data => {
            if(data.data === "0"){
                alert("대회 참여가 등록되었습니다.");
                ContestRepository.getContestList()
                .then(data => this.contestList = data)
                .catch(err => { console.log("ContestListStore err : " , err)});
            }else if(data.data === "2"){
                alert("대회 미지정.");
            }
        })
        .catch(err => { console.log("ContestListStore err : " , err)});
   }
}