import { observable, action } from 'mobx';
import LoginRepository from '../repository/LoginRepository';

export default class HeaderStore {
  @observable header_display = '';
  @observable login_display = 'none';
  @observable logout_display = '';
  @observable contestDetailFlag = "0";
  @observable hunter_id = '';
  @observable hunter_name = '';
  @observable isLogin = false;
  @observable isProgress=false;

  constructor(root){
    this.root = root;
  }

  @action setLoginData = (hunter_id, hunter_name, isLogin) => {
    this.hunter_id = hunter_id;
    this.hunter_name = hunter_name;
    this.isLogin = isLogin;
  }
  @action headerShow = () => {
    this.header_display = '';
  }

  @action headerHide = () => {
    this.header_display = 'none';
  }

  @action login = () => {
    this.login_display = '';
    this.logout_display = 'none';
  }

  @action logout = () => {
    this.login_display = 'none';
    this.logout_display = '';
  }

  commonHeaderShow = () => {
    document.body.classList.add('is-switching');
    document.body.classList.remove('is-article-visible');
    this.root.mainStore.mainHide();
    this.headerShow();
    this.root.footerStore.footerShow();
    setTimeout(function() {
      document.body.classList.remove('is-switching');
    }, (300));
  }
  commonHeaderHide = () => {
    document.body.classList.add('is-switching');
    document.body.classList.add('is-article-visible');
    this.headerHide();
    this.root.mainStore.mainShow();
    setTimeout(function() {
      document.body.classList.remove('is-switching');
      }, (300));
  }

  commonMainHide = () => {
    this.root.contestStore.contestHide();
    this.root.contestListStore.contestListHide();
    this.root.contestDetailStore.contestDetailHide();
    this.root.registStore.registHide();
    this.root.loginStore.loginHide();
  }

  @action eventMainHide = () => {
    if(this.contestDetailFlag === "1"){
      this.eventContestDetailHide();
    }else if(this.contestDetailFlag === "2"){
      this.eventContestRecordHide();
    }else{
      this.commonMainHide();
      this.commonHeaderShow();
      //this.contestDetailFlag = "0";
    }
  }

  @action eventContestDetailHide = () => {
    this.contestDetailFlag = "0";
    this.root.contestDetailStore.contestDetailHide();
    this.root.contestListStore.contestListShow();
  }

  @action eventContestRecordHide = () => {
    this.contestDetailFlag = "1";
    this.root.contestRecordStore.contestRecordHide();
    this.root.contestDetailStore.contestDetailShow();
  }

  @action appContestShow = () => {
    this.commonHeaderHide();
    this.root.contestStore.contestShow();
  }
  @action appContestListShow = () => {
    this.root.contestListStore.getContestList(); 
    this.commonHeaderHide();
    this.root.contestListStore.contestListShow();
  }
  @action appContestDetailShow = (contest_id) => {
    this.contestDetailFlag = "1";
    this.root.contestListStore.contestListHide();
    this.root.contestDetailStore.getContestDetail(contest_id);
    this.commonHeaderHide();
    this.root.contestDetailStore.contestDetailShow();
  }
  @action appContestRecordShow = (contest_id) => {
    this.contestDetailFlag = "2";
    this.root.contestDetailStore.contestDetailHide();
    this.root.contestRecordStore.contest_id = contest_id;
    this.commonHeaderHide();
    this.root.contestRecordStore.contestRecordShow();
  }
  
  @action appRegistShow = () => {
    this.commonHeaderHide();
    this.root.registStore.registShow();
  }
  @action appLoginShow = () => {
    this.commonHeaderHide();
    this.root.loginStore.loginShow();
  }

  @action eventLogout = (e) => {
    e.preventDefault();
    e.stopPropagation();
    if(window.confirm("로그아웃 하시겠습니까?")){
      LoginRepository.logout()
        .then(data => {
          if(data.data === "0"){
            alert("로그아웃 되었습니다.");
            this.setLoginData("" , "", false);
            this.commonHeaderShow();
          }else{
            alert("?");
          }
        })
        .catch(err => { console.log("eventLogout err : " , err)});
    }
  }

  @action checkLogin = () => {
    LoginRepository.checkLogin()
      .then(data => {
        if(data.data === "0"){
          this.setLoginData(data.hunter_id, data.hunter_name, data.isLogin);
          this.eventMainHide();
        }
      })
      .catch(err => { console.log("isLogin err : " , err)});
  }
}