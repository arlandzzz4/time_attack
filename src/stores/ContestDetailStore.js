import { observable, action } from 'mobx';
import {asyncAction} from "mobx-utils"
import ContestRepository from '../repository/ContestRepository';

export default class ContestDetailStore {
  @observable article_class = '';
  @observable article_display = 'none';
  @observable contestDetail;
  @observable chk = 'Y';
  constructor(root){
    this.root = root;
  }

  @action contestDetailShow = () => {
    setTimeout(() => {
      this.article_class = 'active';
    }, 300);
    this.article_display = '';
  }

  @action contestDetailHide = () => {
    this.article_class = '';
    this.article_display = 'none';
  }

  @asyncAction
  getContestDetail = (contest_id) => {
    if(contest_id)
     ContestRepository.getContestDetail(contest_id)
        .then(data => {
            this.contestDetail = data.data;
            this.chk = data.chk;
          })
        .catch(err => { console.log("ContestDetailStore getContestDetail err : " , err)});
   }

   @asyncAction
   updateRecord = (e, contest_id, hunter_id, hunter_name, record_m, record_s, record_ms, weapon) => {
    if(contest_id)
     ContestRepository.updateRecord(contest_id, hunter_id, hunter_name, record_m, record_s, record_ms, weapon)
        .then(data => {
          })
        .catch(err => { console.log("ContestDetailStore updateRecord err : " , err)});
   }
}