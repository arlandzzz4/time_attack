import { observable, action } from 'mobx';
import LoginRepository from '../repository/LoginRepository';
import { properties } from '../etc/properties.js';
const crypto = require('crypto');

export default class LoginStore {
  @observable article_class = 'close';
  @observable article_display = 'none';
  @observable hunterId = '';
  @observable password = '';


  constructor(root){
    this.root = root;
  }

  @action loginShow = () => {
    setTimeout(() => {
      this.article_class = 'active';
    }, 300);
    this.article_display = '';
  }

  @action loginHide = () => {
    this.article_class = '';
    this.article_display = 'none';
  }

  @action changeHunterId = (e) => {
    this.hunterId = e.target.value;
  }

  @action changePassword = (e) => {
    this.password = e.target.value;
  }

  @action eventlogin = (e, headerStore) => {
    e.preventDefault();
    e.stopPropagation();

    if(this.hunterId === null || this.hunterId === ""){
      alert("헌터 ID를 입력해주세요.");
      return false;
    }
    if(this.password === null || this.password === ""){
      alert("패스워드를 입력해 주세요.");
      return false;
    }

    const pass = crypto.pbkdf2Sync(this.password, properties.salt, 250, 64, 'sha512').toString('base64');

    LoginRepository.login(this.hunterId, pass)
        .then(data => {
          if(data.data === "0"){
            alert("로그인 되었습니다.");
            headerStore.setLoginData(data.hunter_id, data.hunter_name, data.isLogin);
            headerStore.eventMainHide();
          } else if(data.data === "1"){
            alert("헌터ID나 패스워드가 잘못되었습니다.");
          } else if(data.data === "2"){
            alert("이미 로그인 되어 있습니다.");
            headerStore.setLoginData(data.hunter_id, data.hunter_name, data.isLogin);
            headerStore.eventMainHide();
          }
        })
        .catch(err => { console.log("LoginRepository err : " , err)});
  }
}