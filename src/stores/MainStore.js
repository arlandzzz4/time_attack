import { observable, action } from 'mobx';

export default class MainStore {
  @observable main_display = 'none';
  constructor(root){
    this.root = root;
  }

  @action mainShow = () => {
    this.main_display = '';
  }

  @action mainHide = () => {
    this.main_display = 'none';
  }
}