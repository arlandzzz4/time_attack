const mongoose = require('mongoose');

const countersSchema = new mongoose.Schema({
  _id: String  
  ,seq : Number
}, { collection : 'counters' });

countersSchema.statics.getSeq = function (name) {
  return this.collection.findAndModify(
        { "_id": name },
        [],
        { "$inc": { "seq": 1 } },
        { "new": true, "upsert": true }
     );
};

module.exports = mongoose.model('counters', countersSchema);