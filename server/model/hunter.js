const mongoose = require('mongoose');

const hunterSchema = new mongoose.Schema({
    hunter_id : {type: String},
    hunter_name : {type: String},
    password : {type: String},
    registration_date : {type: Date},
    update_date : {type: Date, default: Date.now}
}, { collection : 'hunter' });

// Create new contest document
hunterSchema.statics.create = function (hunterInfo) {
  console.log("create : ", hunterInfo);
    // this === Model
    const hunterSchema = new this(hunterInfo);
    // return Promise
    return hunterSchema.save();
  };

// Find All
hunterSchema.statics.findAll = function () {
    return this.find({});
};

// Find One by hunter_id
hunterSchema.statics.findOneByHunterid = function (hunter_id) {
    return this.findOne({hunter_id:hunter_id},{ _id: false });
  };

  hunterSchema.statics.findOneByLogin = function (hunter_id, password) {
    return this.findOne({hunter_id:hunter_id,password:password},{ _id: false });
  };

module.exports = mongoose.model('hunter', hunterSchema);