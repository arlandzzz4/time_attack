const express = require("express");
//1. mongoose 모듈 가져오기
var mongoose = require('mongoose');

console.log("db host : " + process.env.DBURL);

mongoose.set('useUnifiedTopology', true);
// 2. testDB 세팅
mongoose.connect('mongodb://' + process.env.DBURL, { useNewUrlParser: true });

// 3. 연결된 testDB 사용
var db = mongoose.connection;
// 4. 연결 실패
db.on('error', function(error){
    console.log('Connection Failed! : ', error);
});
// 5. 연결 성공
db.once('open', function() {
    console.log('Connected!');
});
