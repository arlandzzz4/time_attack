var express = require('express');
const router = express.Router();
const Hunter = require('../model/hunter.js');


router.post('/', function(req, res){
  console.log('routes hunter.js /');
});

router.post('/registHunter', function(req, res){
  const hunterId = req.body.hunterId;
  const hunterName = req.body.hunterName;
  const password = req.body.password;
  if(hunterId && hunterId !== null && hunterId !== ""){
    //id체크
    Hunter.findOneByHunterid(hunterId).count()
      .then((cnt) => {
        if(cnt === 0){
          Hunter.create({ hunter_id:hunterId
            , hunter_name:hunterName
            , password:password
            , registration_date:new Date().toISOString().substring(0,10).replace(/-/gi,'')
            , update_date:new Date().toISOString().substring(0,10).replace(/-/gi,'')
          })
          .then(() => {
            res.json({"status":200,"data":"0"});
          })
          .catch(err => {
            res.json({"status":500,"data":err});
          });
        }else {
          res.json({"status":200,"data":"1"});
        }
      })
      .catch(err => res.status(500).send(err));	
  }else{
    res.json({"status":200,"data":"2"});
  }
});


module.exports = router;