const express = require('express');
const router = express.Router();
const Hunter = require('../model/hunter.js');
//const session = require('express-session');

router.post('/', function(req, res){
  console.log('routes common.js /');
});

router.post('/login', function(req, res){
  const hunter_id = req.body.hunter_id;
  const pw = req.body.password;
  if(req.session && req.session.isLogin){
    res.json({"status":200,"data":"2","isLogin":true,"hunter_id":req.session.hunter_id,"hunter_name":req.session.hunter_name});
  }else{
    Hunter.findOneByLogin(hunter_id, pw).then((hunter) => {
      if(hunter){
        req.session.hunter_id = hunter.hunter_id;
        req.session.hunter_name = hunter.hunter_name;
        req.session.createCurTime = new Date().toISOString().substring(0,10).replace(/-/gi,'');
        req.session.isLogin = true;
        req.session.save();
        res.json({"status":200,"data":"0","isLogin":req.session.isLogin,"hunter_id":req.session.hunter_id,"hunter_name":req.session.hunter_name});
      }else {
        res.json({"status":200,"data":"1","isLogin":false});
      }
    })
  }
});

router.post('/checkLogin', function(req, res){
  if(req.session && req.session.isLogin){
    res.json({"status":200,"data":"0","isLogin":true,"hunter_id":req.session.hunter_id,"hunter_name":req.session.hunter_name});
  }else{
    res.json({"status":200,"data":"1","isLogin":false});
  }
});

router.post('/logout', function(req, res){
  if(req.session.isLogin){
    req.session.destroy(function(err){
      if(err){
          console.log(err);
          res.status(500).send(err);
      }else{
          res.json({"status":200,"data":"0","isLogin":false});
      }
    })
  }else
    res.json({"status":200,"data":"0","isLogin":false});
});


module.exports = router;