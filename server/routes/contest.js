var express = require('express');
const router = express.Router();
const Counters = require('../model/counters.js');
const Contest = require('../model/contest.js');


router.get('/', function(req, res){
  console.log('routes contest.js /');
});

router.get('/getContestList', function(req, res){
	Contest.findAll()
    .then((contestList) => {
      if (!contestList.length) return res.status(404).send({ err: 'ContestList not found' });
      res.send(contestList);
    })
    .catch(err => res.status(500).send(err));	
});

router.get('/getContestDetail', function(req, res){
	Contest.findDetail(req.query.contest_id)
    .then((contest) => {
      if (!contest) return res.status(404).send({ err: 'Contest not found' });
      if(contest.length === 0){

        Contest.find({ contest_id : req.query.contest_id})
        .then((contestOne) => {
          //res.send(contestOne);
          res.json({"status":200,"data":contestOne, "chk":"N"});
        }).catch(err => res.status(500).send(err));
      }else{
        //res.send(contest);
        res.json({"status":200,"data":contest, "chk":"Y"});
      }
    })
    .catch(err => res.status(500).send(err));	
});

router.post('/registContest', function(req, res){
  const contestTitle = req.body.contestTitle;
  const multiplayer = req.body.multiplayer;
  const contestStart = req.body.contestStart;
  const contestEnd = req.body.contestEnd;
  const description = req.body.description;
  const hunter_id = req.session.hunter_id;
  const hunter_name = req.session.hunter_name;
  if(hunter_id && hunter_id !== null && hunter_id !== ""){
    Counters.getSeq("contest_id")
    .then(counters => {
      Contest.create({
        contest_id: counters.value.seq
        , contest_title:contestTitle
        , multiplayer:multiplayer
        , contest_start:contestStart.substr(0,10)
        , contest_end:contestEnd.substr(0,10)
        , description:description
        , registration_date:new Date().toISOString().substring(0,10).replace(/-/gi,'')
        , registration_id:hunter_id
        , update_date:new Date().toISOString().substring(0,10).replace(/-/gi,'')
        , update_id:hunter_id
      })
      .then(() => {
        res.json({"status":200,"data":"0"});
      })
      .catch(err => {
        res.json({"status":500,"data":err});
      });
    })
    .catch(err => {
      res.json({"status":500,"data":err});
    });
    
  }else{
    res.json({"status":200,"data":"2"});
  }
});

router.post('/contestEnter', function(req, res){
  const contest_id = req.body.contest_id;
  const hunter_id = req.session.hunter_id;
  const hunter_name = req.session.hunter_name;
  if(contest_id && contest_id !== null && contest_id !== ""){
    Contest.contestEnter(contest_id
      , hunter_id
      , hunter_name)
    .then(() => {
      res.json({"status":200,"data":"0"});
    })
    .catch(err => {
      console.log("err : ", err);
      res.json({"status":500,"data":err});
    });
  }else{
    res.json({"status":200,"data":"2"});
  }
});

router.post('/updateRecord', function(req, res){
  const contest_id = req.body.contest_id;
  const hunter_id = req.session.hunter_id;
  const hunter_name = req.session.hunter_name;
  const record_m = req.body.record_m;
  const record_s = req.body.record_s;
  const record_ms = req.body.record_ms;
  const weapon = req.body.weapon;
  const url = req.body.url;
  if(contest_id && contest_id !== null && contest_id !== ""){
    Contest.updateRecord( contest_id
      , hunter_id
      , hunter_name
      , record_m
      , record_s
      , record_ms
      , weapon
      , url
    )
    .then(() => {
      res.json({"status":200,"data":"0"});
    })
    .catch(err => {
      res.json({"status":500,"data":err});
    });
  }else{
    res.json({"status":200,"data":"2"});
  }
});

module.exports = router;